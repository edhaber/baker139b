using System;
using Baker139.CBES;
using Baker139B;

namespace Baker139B
{
    public interface IUpdateSystem : ISystem
    {
        void Update(World world, double ms);
    }
}

