// Released to the public domain. Use, modify and relicense at will.
using System;

using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Audio;
using OpenTK.Audio.OpenAL;
using OpenTK.Input;
using System.Collections.Generic;
using System.Drawing;
using Baker139B.Graphics;
using Baker139B.Assets.Model;
using Baker139.CBES;
using Baker139B.Systems;
using Baker139B.UI;
using Baker139B.Utils;
using Baker139;
using Baker139B.Rendering;
using Baker139B.Common;

namespace Baker139B
{
    public class Game : GameWindow
    {
        private const int WORLD_WIDTH = 40;
        private const int WORLD_HEIGHT = 20;
        private float TileDimension;
        private float TileHeightCount;
        private bool FixedTileCount;
        private readonly World world;
        private readonly Logic logic;
        private readonly UIState uiState;
        private readonly AssetManager assetManager;
        private readonly TextureCollection textureCollection;
        private readonly IList<IUpdateSystem> updateSystems;
        private readonly IList<IRenderSystem> renderSystems;
        private readonly UISystem uiSystem;

        public Game(int width, int height) : base(width, height)
        {
            this.FixedTileCount = true;
            this.TileHeightCount = 20;
            this.TileDimension = 8;
            this.world = new World();
            this.logic = new Logic(world, WORLD_WIDTH, WORLD_HEIGHT);
            
            this.textureCollection = new TextureCollection();
            
            this.assetManager = new AssetManager();
            
            this.updateSystems = new List<IUpdateSystem>();
            this.updateSystems.Add(new MovementSystem());
            this.updateSystems.Add(new AnimationSystem(this.assetManager));
            this.updateSystems.Add(new PathFollowingSystem(WORLD_WIDTH, WORLD_WIDTH));
            this.updateSystems.Add(new WeaponSystem());
            this.updateSystems.Add(new HealthSystem());
            
            
            this.renderSystems = new List<IRenderSystem>();
            this.renderSystems.Add(new SelectedUnitRenderSystem());
            this.renderSystems.Add(new TextureRenderSystem(this.assetManager, this.textureCollection));
            this.renderSystems.Add(new HealthBarRenderSystem());
            
            this.uiState = new UIState(this.world, this.logic);
            this.uiSystem = new UISystem(this.uiState, width, height);
            
            this.world.EntityAdded += this.EntityAdded;
            this.world.EntityRemoved += this.EntityRemoved;
        }
        
        private void LoadAssets()
        {
            
        }
        
        /// <summary>Load resources here.</summary>
        /// <param name="e">Not used.</param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            
            // Loading assets
            this.assetManager.LoadFile("Content/marine.json");
            this.assetManager.LoadFile("Content/terrain.json");
            
            foreach (var textureFileName in this.assetManager.Animations.RequiredTextures)
            {
                this.textureCollection.LoadTexture(textureFileName);
                OpenGLUtils.CheckError(String.Format("Load of file '{0}'", textureFileName));
            }
            
            // Event Hookups
            this.Mouse.ButtonDown += this.MouseButtonDown;
            this.Mouse.ButtonUp += this.MouseButtonUp;
            this.Mouse.Move += this.MouseMove;
            
            // Finally, start the game
            this.logic.StartGame();
        }
        
        private WorldCoord WindowToGame(Point point)
        {
            
            if (this.FixedTileCount)
            {
                return new WorldCoord(point.X / ((ClientRectangle.Height / this.TileHeightCount)), (ClientRectangle.Height - point.Y) / (ClientRectangle.Height / this.TileHeightCount));
            }
            else
            {
                return new WorldCoord(point.X / this.TileDimension, (ClientRectangle.Height - point.Y) / this.TileDimension);
            }
        }

        private PointF GameToTile(PointF point)
        {
            return new PointF((float)Math.Floor(point.X), (float)Math.Floor(point.Y));
        }
        
        public void MouseButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.Button == MouseButton.Left)
            {
                this.uiState.OnLeftDown(WindowToGame(e.Position));
            }
            else if (e.Button == MouseButton.Right)
            {
                this.uiState.OnRightDown(WindowToGame(e.Position));
            }
        }

        public void MouseButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (e.Button == MouseButton.Left)
            {
                this.uiState.OnLeftUp(WindowToGame(e.Position));
            }
            else if (e.Button == MouseButton.Right)
            {
                this.uiState.OnRightUp(WindowToGame(e.Position));
            }
        }

        protected void MouseMove(object sender, MouseMoveEventArgs e)
        {
            this.uiState.OnMouseLocationUpdate(WindowToGame(e.Position));
        }

        /// <summary>
        /// Called when your window is resized. Set your viewport here. It is also
        /// a good place to set up your projection matrix (which probably changes
        /// along when the aspect ratio of your window).
        /// </summary>
        /// <param name="e">Not used.</param>
        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);

            SetupViewPort ();
            this.uiSystem.OnResize (this.Width, this.Height);
        }

        protected void SetupViewPort ()
        {
            GL.Viewport (0, 0, this.Width, this.Height);

            GL.MatrixMode (MatrixMode.Projection);
            GL.LoadIdentity ();
            //GL.Ortho (0, this.Width, this.Height, 0, 0, 1);
            if (this.FixedTileCount) {
                GL.Ortho (0, ((double)ClientRectangle.Width / ClientRectangle.Height) * this.TileHeightCount, 0, this.TileHeightCount, -1000, 1000);
            } // Fixed vertical tiles
            else {
                GL.Ortho (0, (double)ClientRectangle.Width / this.TileDimension, 0, (double)ClientRectangle.Height / this.TileDimension, -1000, 1000);
            } // Fixed tile size
            GL.MatrixMode (MatrixMode.Modelview);
            GL.Enable (EnableCap.Texture2D);
            GL.Enable (EnableCap.Blend);
            GL.BlendFunc (BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
        }
        /// <summary>
        /// Called when it is time to setup the next frame. Add you game logic here.
        /// </summary>
        /// <param name="e">Contains timing information for framerate independent logic.</param>
        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            base.OnUpdateFrame(e);
            
            if (Keyboard[Key.Escape])
            {
                Exit();
            }
            
            foreach (var system in this.updateSystems)
            {
                system.Update(this.world, e.Time);
            }
        }
        
        /// <summary>
        /// Called when it is time to render the next frame. Add your rendering code here.
        /// </summary>
        /// <param name="e">Contains timing information.</param>
        protected override void OnRenderFrame(FrameEventArgs e)
        {
            base.OnRenderFrame(e);
            SetupViewPort ();
            GL.MatrixMode(MatrixMode.Modelview);
            GL.ClearColor(Color.Gray);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            
            var renderer = new Renderer();
            
            foreach (var system in this.renderSystems)
            {
                system.Render(this.world, renderer);
                OpenGLUtils.CheckError(String.Format("After render of '{0}'", system.GetType().Name.ToString()));
            }
            
            this.uiSystem.Render(renderer);
            OpenGLUtils.CheckError(String.Format("After render of '{0}'", this.uiSystem.GetType().Name.ToString()));
            
            renderer.Render();
            SwapBuffers();
        }

        private void EntityAdded(World world, Entity entity)
        {
            foreach (var system in this.updateSystems)
            {
                system.EntityAdded(world, entity);
            }
            
            foreach (var system in this.renderSystems)
            {
                system.EntityAdded(world, entity);
            }
        }
        
        void EntityRemoved(World world, Entity entity)
        {
            foreach (var system in this.updateSystems)
            {
                system.EntityRemoved(world, entity);
            }
            
            foreach (var system in this.renderSystems)
            {
                system.EntityRemoved(world, entity);
            }
        }
    }
}