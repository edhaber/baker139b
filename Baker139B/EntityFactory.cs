using System;
using Baker139.CBES;
using Baker139B.Components;
using OpenTK;
using Baker139B.Common;

namespace Baker139B
{
    public class EntityFactory
    {
        public static Entity BuildMarine(World world, WorldCoord startLocation)
        {
            return world.BuildEntity(
                new Location() { CurrentLocation = startLocation },
                new Movement(),
                new Textured() { Layer = GraphicsLayer.Units },
                new Animated(),
                new Selectable(),
                new PathFollower(),
                new Weaponized(10, 6, 1000),
                new Health(60)
            );
        }
        
        public static Entity BuildDirt(World world, WorldCoord location)
        {
            var entity = world.BuildEntity(
                new Location() { CurrentLocation = location },
                new Textured() {
                    CurrentAnimation = "Terrain.Dirt",
                    Layer = GraphicsLayer.Terrain,
                }
            );
            return entity;
        }

        public static Entity BuildWall(World world, WorldCoord location)
        {
            return world.BuildEntity(
                new Location() { CurrentLocation = location },
                new Textured() { CurrentAnimation = "Terrain.Wall", Layer = GraphicsLayer.Terrain },
                new Obstacle()
            );
        }
    }
}

