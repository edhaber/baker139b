using System;
using OpenTK.Graphics.OpenGL;
using OpenTK;
using System.Drawing;
using Baker139B.Assets.IF;
using System.Collections.Generic;
using Baker139.CBES;
using Baker139B.Components;
using Baker139B.Rendering;

namespace Baker139B.UI
{
    public class UISystem
    {
        private readonly UIState uiState;
        int width;
        int height;
        Bitmap text_bmp;
        int text_texture;
        Font sans;
        
        public UISystem(UIState uiState, int width, int height)
        {
            this.uiState = uiState;
            this.width = width;
            this.height = height;
            sans = new Font (FontFamily.GenericSansSerif, 24);

            // Create Bitmap and OpenGL texture
            text_bmp = new Bitmap (width, height); // match window size

            text_texture = GL.GenTexture ();
            GL.BindTexture (TextureTarget.Texture2D, text_texture);
            GL.TexParameter (TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)All.Linear);
            GL.TexParameter (TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)All.Linear);
            GL.TexImage2D (TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, text_bmp.Width, text_bmp.Height, 0,
                PixelFormat.Bgra, PixelType.UnsignedByte, IntPtr.Zero); // just allocate memory, so we can update efficiently using TexSubImage2D

            // Render text using System.Drawing.
            // Do this only when text changes.
            using (System.Drawing.Graphics gfx = System.Drawing.Graphics.FromImage (text_bmp)) {
                gfx.Clear (Color.Transparent);
                gfx.DrawString ("This is a TEST",sans,Brushes.White,new PointF(10,10)); // Draw as many strings as you need
            }
            System.Drawing.Imaging.BitmapData data = text_bmp.LockBits (new Rectangle(0,0,width,height),
                        System.Drawing.Imaging.ImageLockMode.ReadOnly,
                        System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            GL.BindTexture (TextureTarget.Texture2D, text_texture);
            GL.TexSubImage2D (TextureTarget.Texture2D, 0,
                0, 0, width, height,
                PixelFormat.Bgra, PixelType.UnsignedByte, data.Scan0);

            text_bmp.UnlockBits (data);

        }

        public void Render(Renderer renderer)
        {
            // Draw area selection box
            renderer.AddRenderAction(GraphicsLayer.UI, () => this.RenderUI());
        }
        
        private void RenderUI()
        {
            GL.Disable(EnableCap.Texture2D);
            var selectedArea = this.uiState.SelectedArea;
            if (selectedArea != null)
            {
                GL.Color3(Color.Green);
            
                float left = Math.Min(selectedArea.Item1.X, selectedArea.Item2.X);
                float right = Math.Max(selectedArea.Item1.X, selectedArea.Item2.X);
                float bottom = Math.Min(selectedArea.Item1.Y, selectedArea.Item2.Y);
                float top = Math.Max(selectedArea.Item1.Y, selectedArea.Item2.Y);
            
                GL.Begin(BeginMode.LineLoop);
                GL.Vertex2(left, top);
                GL.Vertex2(right, top);
                GL.Vertex2(right, bottom);
                GL.Vertex2(left, bottom);
                GL.End();
            }

            GL.Begin(BeginMode.Lines);
            GL.Color3(1.0f, 1.0f, 1.0f); 
            for (float x =0.0f; x<150.0f; x+=1.0f)
            {
                if (x % 10 == 0)
                {
                    GL.Color4(1.0, 1.0, 1.0, 0.5f);
                }
                else
                {
                    GL.Color4(0.0f, 0.0f, 1.0f, 0.5f);
                } 

                GL.Vertex3(x, 0.0f, 0.0f);
                GL.Vertex3(x, 150.0f, 0.0f);
            }
            for (float y =0.0f; y<150.0f; y+=1.0f)
            {
                if (y % 10 == 0)
                {
                    GL.Color4(1.0, 1.0, 1.0, 0.5f);
                }
                else
                {
                    GL.Color4(0.0f, 0.0f, 1.0f, 0.5f);
                } 
                GL.Vertex3(0.0f, y, 0.0f);
                GL.Vertex3(150.0f, y, 0.0f);
            }
            GL.End();

            GL.MatrixMode (MatrixMode.Projection);
            GL.LoadIdentity ();
            GL.Ortho (0, width, height, 0, -1, 1);

            GL.Enable (EnableCap.Texture2D);
            GL.Enable (EnableCap.Blend);
            GL.BlendFunc (BlendingFactorSrc.One, BlendingFactorDest.OneMinusSrcAlpha);
            GL.BindTexture (TextureTarget.Texture2D, this.text_texture);
            GL.Begin (BeginMode.Quads);
            GL.TexCoord2 (0f, 0f); GL.Vertex2 (0f, 0f);
            GL.TexCoord2 (1f, 0f); GL.Vertex2 (width, 0f);
            GL.TexCoord2 (1f, 1f); GL.Vertex2 (width, height);
            GL.TexCoord2 (0f, 1f); GL.Vertex2 (0f, height);
            GL.End ();
        }

        internal void OnResize (int Width, int Height)
        {
            this.width = Width;
            this.height = Height;
            // Ensure Bitmap and texture match window size
           /* text_bmp.Dispose ();
            text_bmp = new Bitmap (this.width, this.height);

            GL.BindTexture (TextureTarget.Texture2D ,text_texture);
            GL.TexSubImage2D (TextureTarget.Texture2D, 0, 0, 0, text_bmp.Width, text_bmp.Height,
                PixelFormat.Bgra, PixelType.UnsignedByte, IntPtr.Zero);*/
        }
   }
}

