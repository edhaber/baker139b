using System;
using OpenTK;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Baker139B.Assets.IF;
using Baker139.CBES;
using Baker139B.Components;
using Baker139B.Utils;
using Baker139B.Common;

namespace Baker139B.UI
{
    public class UIState
    {
        private World world;
        private Logic logic;
        
        private WorldCoord? selectedAreaStart;
        private WorldCoord? selectedAreaEnd;
        
        public UIState(World world, Logic logic)
        {
            this.world = world;
            this.logic = logic;
            
            this.selectedAreaStart = null;
            this.selectedAreaEnd = null;
        }
        
        public Tuple<WorldCoord, WorldCoord> SelectedArea
        {
            get
            {
                if (this.selectedAreaStart == null) return null;
                
                return new Tuple<WorldCoord, WorldCoord>(this.selectedAreaStart.Value, this.selectedAreaEnd.Value);
            }
        }

        // TODO: Perhaps this should go in another class?
        public void OnLeftDown(WorldCoord location)
        {
            this.selectedAreaStart = location;
            this.selectedAreaEnd = location;
        }
        
        public void OnMouseLocationUpdate(WorldCoord location)
        {
            if (this.selectedAreaStart != null)
            {
                this.selectedAreaEnd = location;
            }
        }
        
        public void OnLeftUp(WorldCoord location)
        {
            if (this.selectedAreaStart != null)
            {
                DeselectUnits();
                SelectNewUnits(this.selectedAreaStart.Value, location);
                                                       
                this.selectedAreaStart = null;
                this.selectedAreaEnd = null;   
            }
        }
        
        private void DeselectUnits()
        {
            var entities = from entity in world.FindEntities()
                    where entity.HasComponent<Selectable>()
                        && entity.GetComponent<Selectable>().IsSelected
                    select entity;
                
            foreach (var entity in entities)
            {
                entity.GetComponent<Selectable>().IsSelected = false;
            }
        }
        
        private void SelectNewUnits(WorldCoord startLocation, WorldCoord endLocation)
        {
            var entities = from entity in this.world.FindEntities()
                where entity.HasComponent<Selectable>()
                    && entity.HasComponent<Location>()
                    && SpatialUtils.LocationInArea(entity.GetComponent<Location>().CurrentLocation, startLocation, endLocation)
                select entity;
            
            foreach (var entity in entities)
            {
                entity.GetComponent<Selectable>().IsSelected = true;
            }
        }
        
        public void OnRightDown(WorldCoord location)
        {
        }
        
        public void OnRightUp(WorldCoord location)
        {
            var selectedUnits = from entity in this.world.FindEntities()
                where entity.HasComponent<Selectable>()
                    && entity.GetComponent<Selectable>().IsSelected
                select entity.ID;
            
            this.logic.Move(selectedUnits, new WorldCoord(location.X, location.Y));
        }
    }
}

