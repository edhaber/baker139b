using System;
using System.Collections.Generic;
using System.Drawing;
using OpenTK;
using Baker139.CBES;
using Baker139B.Components;
using Baker139B.Common;

namespace Baker139B
{
    public class Logic
    {
        private World world;
        private readonly int worldWidth;
        private readonly int worldHeight;
        
        public Logic(World world, int worldWidth, int worldHeight)
        {
            this.world = world;
            this.worldWidth = worldWidth;
            this.worldHeight = worldHeight;
        }

        public void StartGame()
        {
            EntityFactory.BuildMarine(this.world, new WorldCoord(4, 10));
            EntityFactory.BuildMarine(this.world, new WorldCoord(8, 10));
            EntityFactory.BuildMarine(this.world, new WorldCoord(15, 15));
            EntityFactory.BuildMarine(this.world, new WorldCoord(16, 2));
            EntityFactory.BuildMarine(this.world, new WorldCoord(13, 8));
            
            var random = new Random();
            
            for (int x = 0; x < this.worldWidth; x++)
            {
                for (int y = 0; y < this.worldHeight; y++)
                {
                    var location = new WorldCoord(x + 0.5f, y + 0.5f);
                    if (random.Next(100) < 10)
                    {
                        EntityFactory.BuildWall(this.world, location);
                    }
                    else
                    {
                        EntityFactory.BuildDirt(this.world, location);
                    }
                }
            }
        }
        
        public void Move(IEnumerable<int> entityIDs, WorldCoord newTarget)
        {
            foreach (var entityID in entityIDs)
            {
                var entity = this.world.GetEntity(entityID);
                entity.GetComponent<PathFollower>().ClearDestination();
                entity.GetComponent<PathFollower>().FinalDestination = newTarget;
            }
        }
        
        
    }
}

