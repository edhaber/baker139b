using System;
using Baker139.CBES;

namespace Baker139B
{
    public interface ISystem
    {
        void EntityAdded(World world, Entity entity);

        void EntityRemoved(World world, Entity entity);
    }
}

