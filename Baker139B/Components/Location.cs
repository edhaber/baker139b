using System;
using Baker139.CBES;
using Baker139B.Common;

namespace Baker139B.Components
{
    public class Location : IComponent
    {
        public WorldCoord CurrentLocation { get; set; }
    }
}

