using System;
using Baker139.CBES;
using Baker139B.Assets.IF;

namespace Baker139B.Components
{
    public class Textured : IComponent
    {
        public Textured()
        {
        }
        
        public string CurrentAnimation { get; set; }
        public int Frame { get; set; }
        public GraphicsLayer Layer { get; set; }
    }
}

