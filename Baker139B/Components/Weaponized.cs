using System;
using Baker139.CBES;

namespace Baker139B.Components
{
    public class Weaponized : IComponent
    {
        private readonly int damage;
        private readonly int range;
        private readonly int cooldownTime;
        private int currentCooldown;
        private int? targetEntityID;
        
        public Weaponized(int damage, int range, int cooldown)
        {
            this.damage = damage;
            this.range = range;
            this.cooldownTime = cooldown;
            this.currentCooldown = cooldown;
        }
        
        public int Damage
        {
            get { return this.damage; }
        }
        
        public int Range
        {
            get { return this.range; }
        }
        
        public int CurrentCooldown
        {
            get { return this.currentCooldown; }
        }
        
        public bool IsOnCooldown
        {
            get { return this.currentCooldown < this.cooldownTime; }
        }
        
        public void StartCooldown()
        {
            this.currentCooldown = 0;
        }
        
        public void AdvanceCurrentCooldown(int duration)
        {
            this.currentCooldown += duration;
        }
        
        public int? TargetEntityID
        {
            get { return this.targetEntityID; }
            set { this.targetEntityID = value; }
        }
    }
}

