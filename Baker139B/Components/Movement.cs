using System;
using Baker139.CBES;
using Baker139B.Common;
using System.Collections.Generic;

namespace Baker139B.Components
{
	public class Movement : IComponent
	{
		private WorldCoord? destination;
		private readonly IList<string> pauseReasons = new List<string> ();
        
		public Movement ()
		{
		}
        
		public bool HasDestination {
			get { return this.destination.HasValue; }
		}
        
		public void ClearDestination ()
		{
			this.destination = null;
		}
        
		public WorldCoord CurrentDestination {
			get {
				return this.destination.Value;
			}
			set {
				this.destination = value;
			}
		}
        
		public bool IsPaused {
			get { return this.pauseReasons.Count > 0; }
		}
        
		public void AddPauseReason (string reason)
		{
			this.pauseReasons.Add (reason);
		}
        
		public void RemovePauseReason (string reason)
		{
			this.pauseReasons.Remove (reason);
		}
	}
}

