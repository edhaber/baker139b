using System;
using Baker139.CBES;

namespace Baker139B.Components
{
    public class Health : IComponent
    {
        private readonly int maxHealth;
        private int currentHealth;
        
        public Health(int maxHealth)
        {
            this.maxHealth = maxHealth;
            this.currentHealth = maxHealth;
        }
        
        public void ChangeHealth(int change)
        {
            this.currentHealth += change;
        }
        
        public int CurrentHealth
        {
            get { return this.currentHealth; }
        }
        
        public int MaxHealth
        {
            get { return this.maxHealth; }
        }
    }
}

