using System;
using Baker139.CBES;

namespace Baker139B.Components
{
    public class Selectable : IComponent
    {
        public Selectable()
        {
        }
        
        public bool IsSelected { get; set; }
    }
}

