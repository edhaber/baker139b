using System;
using Baker139.CBES;
using Baker139B.Common;
using System.Collections.Generic;

namespace Baker139B.Components
{
    /// <summary>
    /// Component for an entity that will be given a final destination, and will have a pathfinder
    /// create a path for it to follow.
    /// 
    /// When the "FinalDestination" property is set, the "Path" property is cleared. A pathfinding
    /// system will notice this, create a path, and set the "Path" property. The first waypoint in the path
    /// will then be set as the "Destination". When the "CurrentDestination" (on the "Destination" component)
    /// is reached, then the "Path" is altered to remove that waypoint, and the next "CurrentDestination" is set.
    /// 
    /// This continues until the "FinalDestination" is achieved, at which point both the "Path" and "FinalDestination"
    /// are cleared (set to null).
    /// 
    /// If a valid "Path" could not be found by the pathfinder, then the "Path" is cleared (null), and the "FinalDestination"
    /// is also cleared.
    /// 
    /// Depends on the "Location" component to store the current location.
    /// Depends on the "Destination" component to help in actually doing the moving.
    /// </summary>
    public class PathFollower : IComponent
    {
        public WorldCoord? FinalDestination { get; set; }
        public IList<WorldCoord> Path { get; set; }
        
        public bool NeedsPath
        {
            get
            {
                return this.FinalDestination.HasValue && this.Path == null;
            }
        }

        public void ClearDestination()
        {
            this.FinalDestination = null;
            this.Path = null;
        }
    }
}

