using System;
using Baker139.CBES;
using Baker139B.Rendering;
using Baker139B;

namespace Baker139B
{
    public interface IRenderSystem : ISystem
    {
        void Render(World world, Renderer renderer);
    }
}

