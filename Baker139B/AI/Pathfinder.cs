using System;
using Baker139B.Common;
using System.Collections.Generic;

namespace Baker139B.AI
{
    /// <summary>
    /// Tracks a 2D grid for obstacles, and comes up with a path
    /// between points when requested
    /// </summary>
    public class Pathfinder
    {
        private readonly int width;
        private readonly int height;
        
        public Pathfinder(int width, int height)
        {
            this.width = width;
            this.height = height;
        }
        
        public void AddObstacle(TileCoord coord)
        {
            // TODO
        }
        
        public void ClearObstacle(TileCoord coord)
        {
            // TODO
        }
        
        /// <summary>
        /// Builds a path from "start" to "end".
        /// </summary>
        /// <returns>
        /// A list of waypoints on the path found. Returns null if a path could
        /// not be found.
        /// </returns>
        /// <param name='start'>
        /// The starting location of the path.
        /// </param>
        /// <param name='end'>
        /// The destination of the path.
        /// </param>
        public IList<WorldCoord> BuildPath(WorldCoord start, WorldCoord end)
        {
            // TODO
            var result = new List<WorldCoord>();
            result.Add(end);
            return result;
        }
    }
}

