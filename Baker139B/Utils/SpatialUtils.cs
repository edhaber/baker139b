using System;
using System.Drawing;
using Baker139B.Components;
using Baker139B.Common;

namespace Baker139B.Utils
{
    public static class SpatialUtils
    {
        public static TileCoord GameWorldToTile(WorldCoord coord)
        {
            return new TileCoord((int)coord.X, (int)coord.Y);
        }
        
        public static bool LocationInArea(WorldCoord location, WorldCoord startCorner, WorldCoord endCorner)
        {
            var left = Math.Min(startCorner.X, endCorner.X);
            var right = Math.Max(startCorner.X, endCorner.X);
            var bottom = Math.Min(startCorner.Y, endCorner.Y);
            var top = Math.Max(startCorner.Y, endCorner.Y);
            
            return left <= location.X &&
                    location.X <= right &&
                    bottom <= location.Y &&
                    location.Y <= top;
        }
    }
}

