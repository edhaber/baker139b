using System;
using OpenTK.Graphics.OpenGL;

namespace Baker139B.Utils
{
    public static class OpenGLUtils
    {
        private const string ERROR_TEMPLATE = "GL Error '{0}' occurred. Cause: {1}";
        
        public static void CheckError(string cause)
        {
            var results = GL.GetError();
            if (results != ErrorCode.NoError)
            {
                var msg = String.Format(ERROR_TEMPLATE, results, cause);
                throw new Exception(msg); 
            }
        }
    }
}

