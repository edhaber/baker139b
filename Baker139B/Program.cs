using System;
using OpenTK.Graphics;
using OpenTK.Platform;

namespace Baker139B
{
    class Program
    {
        [STAThread]
        public static void Main (string[] args)
        {
            var g = new Game(800, 600);
            g.Run();
        }
    }
}
