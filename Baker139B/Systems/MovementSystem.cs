using System;
using System.Linq;
using Baker139.CBES;
using Baker139B.Components;
using OpenTK;
using Baker139;
using Baker139B.Common;

namespace Baker139B.Systems
{
    public class MovementSystem : IUpdateSystem
    {
        private const float ENTITY_SPEED = 5;
        
        public MovementSystem()
        {
        }
        
        public void EntityAdded(World world, Entity e)
        {
        }

        public void EntityRemoved(World world, Entity e)
        {
        }
        
        public void Update(World world, double s)
        {
            var entities =
                from entity in world.FindEntities()
                where entity.HasComponent<Movement>()
                select entity;
                    
            foreach (var entity in entities)
            {
                if (entity.GetComponent<Movement>().HasDestination)
                {
                    var location = entity.GetComponent<Location>().CurrentLocation;
                    var destination = entity.GetComponent<Movement>().CurrentDestination;
                    
                    if (location.Equals(destination))
                    {
                        entity.GetComponent<Movement>().ClearDestination();
                    }
                    else
                    {
                        // Calculate where we would end up in one step
                        var direction = destination - location;
                        direction.Normalize();
                        direction = Vector2.Multiply(direction, (float)s * ENTITY_SPEED);
                        var result = location + direction;
                         
                        // Calculate how far we'd have to move to get to the new location
                        var distanceToTarget = WorldCoord.Distance(destination, location);
                        var distanceToResult = WorldCoord.Distance(result, location);
                         
                        if (distanceToTarget < distanceToResult)
                        {
                            entity.GetComponent<Location>().CurrentLocation = destination;
                            entity.GetComponent<Movement>().ClearDestination();
                        }
                        else
                        {
                            entity.GetComponent<Location>().CurrentLocation = result;
                        }
                    }
                }
            }
        }
    }
}

