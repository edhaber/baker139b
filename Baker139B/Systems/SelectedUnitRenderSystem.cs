using System;
using System.Linq;
using Baker139.CBES;
using Baker139B.Components;
using OpenTK.Graphics.OpenGL;
using System.Drawing;
using Baker139;
using Baker139B.Rendering;

namespace Baker139B.Systems
{
    public class SelectedUnitRenderSystem : IRenderSystem
    {
        public SelectedUnitRenderSystem()
        {
        }
        
        public void EntityAdded(World world, Entity e)
        {
        }

        public void EntityRemoved(World world, Entity e)
        {
        }

        public void Render(World world, Renderer renderer)
        {
            var entities = from entity in world.FindEntities()
                where entity.HasComponent<Selectable>()
                    && entity.GetComponent<Selectable>().IsSelected
                select entity;
            
            foreach (var entity in entities)
            {
                var location = entity.GetComponent<Location>();
                renderer.AddRenderAction(GraphicsLayer.UnitsUI, () => {
                    RenderSelectedUnit(location);
                });
            }
        }
            
        private void RenderSelectedUnit(Location locationComponent)
        {
            GL.MatrixMode(MatrixMode.Modelview);
            GL.Disable(EnableCap.Texture2D);
            GL.Color3(Color.Green);
            
            var location = locationComponent.CurrentLocation;
            GL.Begin(BeginMode.LineLoop);
            float RADIUS = 0.4f;
            float CENTER_Y_OFFSET = -0.4f;
            for (float angle = 0.0f; angle < 360.0f; angle = angle + 1.0f)
            {
                GL.Vertex2(location.X + Math.Sin(angle) * RADIUS, (location.Y + CENTER_Y_OFFSET + Math.Cos(angle) * RADIUS));
            }
            GL.End();
        }
    }
}

