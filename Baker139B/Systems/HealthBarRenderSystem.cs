using System;
using System.Linq;
using Baker139.CBES;
using Baker139B.Rendering;
using Baker139B.Components;
using OpenTK.Graphics.OpenGL;
using System.Drawing;

namespace Baker139B.Systems
{
    public class HealthBarRenderSystem : IRenderSystem
    {
        public HealthBarRenderSystem()
        {
        }

        public void Render(World world, Renderer renderer)
        {
            var entities = from entity in world.FindEntities()
                where entity.HasComponent<Health>()
                select entity;
            
            foreach (var entity in entities)
            {
                var locationComponent = entity.GetComponent<Location>();
                var healthComponent = entity.GetComponent<Health>();
                
                renderer.AddRenderAction(GraphicsLayer.UI, () => {
                    this.RenderHealthBar(locationComponent, healthComponent);
                });
            }
        }
        
        private void RenderHealthBar(Location locationComponent, Health healthComponent)
        {
            GL.MatrixMode(MatrixMode.Modelview);
            GL.Disable(EnableCap.Texture2D);
            
            var location = locationComponent.CurrentLocation;
            float WIDTH = 1f;
            float HEIGHT = 0.2f;
            float CENTER_Y_OFFSET = 0.8f;
            
            var healthyRatio = ((float)healthComponent.CurrentHealth) / ((float)healthComponent.MaxHealth);
            var healthyWidth = WIDTH * healthyRatio;
            
            if (healthyRatio < 0.33)
            {
                GL.Color3(Color.Red);
            }
            else if (healthyRatio >= 0.33 && healthyRatio < 0.66)
            {
                GL.Color3(Color.Yellow);
            }
            else
            {
                GL.Color3(Color.Green);
            }
            // Draw health bar
            GL.Begin(BeginMode.Quads);
            GL.Vertex2(location.X - (WIDTH / 2f), location.Y + CENTER_Y_OFFSET + (HEIGHT / 2f));
            GL.Vertex2(location.X - (WIDTH / 2f) + healthyWidth, location.Y + CENTER_Y_OFFSET + (HEIGHT / 2f));
            GL.Vertex2(location.X - (WIDTH / 2f) + healthyWidth, location.Y + CENTER_Y_OFFSET - (HEIGHT / 2f));
            GL.Vertex2(location.X - (WIDTH / 2f), location.Y + CENTER_Y_OFFSET - (HEIGHT / 2f));
            GL.End();
            
            var damagedRatio = 1.0f - healthyRatio;
            var damagedWidth = WIDTH * damagedRatio;
            GL.Color3(Color.Black);
            // Draw black (damaged) bar
            GL.Begin(BeginMode.Quads);
            GL.Vertex2(location.X + (WIDTH / 2f), location.Y + CENTER_Y_OFFSET + (HEIGHT / 2f));
            GL.Vertex2(location.X + (WIDTH / 2f) - damagedWidth, location.Y + CENTER_Y_OFFSET + (HEIGHT / 2f));
            GL.Vertex2(location.X + (WIDTH / 2f) - damagedWidth, location.Y + CENTER_Y_OFFSET - (HEIGHT / 2f));
            GL.Vertex2(location.X + (WIDTH / 2f), location.Y + CENTER_Y_OFFSET - (HEIGHT / 2f));
            GL.End();
            
            // Outline
            GL.Color3(Color.White);
            GL.Begin(BeginMode.LineLoop);
            GL.Vertex2(location.X - (WIDTH / 2f), location.Y + CENTER_Y_OFFSET + (HEIGHT / 2f));
            GL.Vertex2(location.X + (WIDTH / 2f), location.Y + CENTER_Y_OFFSET + (HEIGHT / 2f));
            GL.Vertex2(location.X + (WIDTH / 2f), location.Y + CENTER_Y_OFFSET - (HEIGHT / 2f));
            GL.Vertex2(location.X - (WIDTH / 2f), location.Y + CENTER_Y_OFFSET - (HEIGHT / 2f));
            GL.End();
            
        }

        public void EntityAdded(World world, Entity entity)
        {
        }

        public void EntityRemoved(World world, Entity entity)
        {
        }
    }
}

