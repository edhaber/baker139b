using System;
using System.Linq;
using Baker139;
using Baker139.CBES;
using Baker139B.Components;
using Baker139B.AI;
using Baker139B.Utils;

namespace Baker139B.Systems
{
    /// <summary>
    /// Path following system.
    /// 
    /// TODO: This is poorly implemented: the actual pathfinder is created on every update. This
    /// should be changed to once, and be able to "hook into" obstacles needing to be added/removed.
    /// However, this "hooking" ability is not yet ready, so we'll use the workaround for now.
    /// </summary>
    public class PathFollowingSystem : IUpdateSystem
    {
        private readonly Pathfinder pathfinder;
        
        public PathFollowingSystem(int gameWidth, int gameHeight)
        {
            this.pathfinder = new Pathfinder(gameWidth, gameHeight);
        }
        
        public void EntityAdded(World world, Entity e)
        {
            if (e.HasComponent<Obstacle>())
            {
                var coord = e.GetComponent<Location>().CurrentLocation;
                this.pathfinder.AddObstacle(SpatialUtils.GameWorldToTile(coord));
            }
        }
        
        public void EntityRemoved(World world, Entity e)
        {
            if (e.HasComponent<Obstacle>())
            {
                var coord = e.GetComponent<Location>().CurrentLocation;
                this.pathfinder.ClearObstacle(SpatialUtils.GameWorldToTile(coord));
            }
        }
        
        public void Update(World world, double ms)
        {
            var pathingEntities =
                from entity in world.FindEntities()
                where entity.HasComponent<PathFollower>()
                select entity;
            
            foreach (var entity in pathingEntities)
            {
                var locationComponent = entity.GetComponent<Location>();
                var pathFollowerComponent = entity.GetComponent<PathFollower>();
                var destinationComponent = entity.GetComponent<Movement>();
                
                this.UpdateEntity(locationComponent, pathFollowerComponent, destinationComponent);
            }
        }
        
        private void UpdateEntity(Location locationComponent,
                                  PathFollower pathFollowerComponent,
                                  Movement movementComponent)
        {
            var currentLocation = locationComponent.CurrentLocation;
            
            if (pathFollowerComponent.NeedsPath)
            {
                var newPath = this.pathfinder.BuildPath(currentLocation, pathFollowerComponent.FinalDestination.Value);
                if (newPath == null)
                {
                    // TODO: Logging
                    pathFollowerComponent.ClearDestination();
                    return;
                }
                else
                {
                    pathFollowerComponent.Path = newPath;
                }
            }
            
            var path = pathFollowerComponent.Path;
            
            if (path == null)
            {
                return;
            }
                
            while (path.Count > 0 && path[0].Equals(currentLocation))
            {
                path.RemoveAt(0);
            }
            
            if (path.Count == 0)
            {
                pathFollowerComponent.ClearDestination();
            }
            else
            {
                movementComponent.CurrentDestination = path[0];
            }
        }
    }
}

