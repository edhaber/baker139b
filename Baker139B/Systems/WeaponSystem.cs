using System;
using System.Linq;
using Baker139.CBES;
using Baker139B.Components;
using Baker139B.Utils;
using Baker139B.Common;

namespace Baker139B.Systems
{
    public class WeaponSystem : IUpdateSystem
    {
        public WeaponSystem()
        {
        }

        public void Update(World world, double ms)
        {
            var entities = from entity in world.FindEntities()
                where entity.HasComponent<Weaponized>()
                select entity;
            
            foreach (var entity in entities)
            {
                var weaponizedComponent = entity.GetComponent<Weaponized>();
                var locationComponent = entity.GetComponent<Location>();
                
                UpdateEntity(entity.ID, weaponizedComponent, locationComponent, world, ms);
            }
        }

        private void UpdateEntity(int id, Weaponized weaponizedComponent, Location locationComponent, World world, double ms)
        {
            if (weaponizedComponent.IsOnCooldown)
            {
                weaponizedComponent.AdvanceCurrentCooldown((int)(ms * 1000));
            }
            else
            {
                var currentLocation = locationComponent.CurrentLocation;
                var weaponRange = weaponizedComponent.Range;
                
                // TODO: Need a better way to determine what an entity would fire at. Right now,
                // I'm looking for "Health". Really, I should be looking for "allegience" or something
                // like that.
                var targets =
                    from entity in world.FindEntities()
                    where entity.HasComponent<Health>() &&
                        entity.ID != id &&
                        WorldCoord.Distance(currentLocation, entity.GetComponent<Location>().CurrentLocation) <= weaponRange
                    orderby WorldCoord.Distance(currentLocation, entity.GetComponent<Location>().CurrentLocation)
                    select entity;
                
                if (targets.Count() > 0)
                {
                    var target = targets.First();
                    var weaponDamage = weaponizedComponent.Damage;
                    target.GetComponent<Health>().ChangeHealth(-1 * weaponDamage);
                    weaponizedComponent.StartCooldown();
                    weaponizedComponent.TargetEntityID = target.ID;
                }
                else
                {
                    weaponizedComponent.TargetEntityID = null;
                }
            }
        }
        
        public void EntityAdded(World world, Entity entity)
        {
        }

        public void EntityRemoved(World world, Entity entity)
        {
        }
    }
}