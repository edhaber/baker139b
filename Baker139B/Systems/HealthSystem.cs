using System;
using System.Linq;
using Baker139.CBES;
using Baker139B.Components;

namespace Baker139B.Systems
{
    public class HealthSystem : IUpdateSystem
    {
        public HealthSystem()
        {
        }

        public void Update(World world, double ms)
        {
            // Since it's possible that we will be removing entities from
            // the game, we will need to do a "to array" here so that we
            // don't end up continuing to iterate over the list of entities
            // after an entity was removed, as that will throw an out of sync
            // error.
            // TODO: Make it so that this isn't necessary?
            var entities = (from entity in world.FindEntities()
                where entity.HasComponent<Health>()
                select entity).ToArray();
            
            foreach (var entity in entities)
            {
                var healthComponent = entity.GetComponent<Health>();
                if (healthComponent.CurrentHealth <= 0)
                {
                    // TODO: Handle death such as animation?
                    world.RemoveEntity(entity);
                }
            }
        }
        
        public void EntityAdded(World world, Entity entity)
        {
        }

        public void EntityRemoved(World world, Entity entity)
        {
        }
    }
}

