using System;
using System.Linq;
using Baker139B.Assets.IF;
using Baker139.CBES;
using Baker139B.Components;
using OpenTK.Graphics.OpenGL;
using System.Drawing;
using Baker139B.Rendering;
using Baker139;

namespace Baker139B.Systems
{
    public class TextureRenderSystem : IRenderSystem
    {
        private readonly IAssetManager assetManager;
        private readonly TextureCollection textures;
        
        public TextureRenderSystem(IAssetManager assetManager, TextureCollection textures)
        {
            this.assetManager = assetManager;
            this.textures = textures;
        }
        
        public void EntityAdded(World world, Entity e)
        {
        }

        public void EntityRemoved(World world, Entity e)
        {
        }
        
        public void Render(World world, Renderer renderer)
        {
            var entities = from entity in world.FindEntities()
                where entity.HasComponent<Textured>()
                select entity;
            
            // Draw units
            foreach (var entity in entities)
            {
                var locationComponent = entity.GetComponent<Location>();
                var textureComponent = entity.GetComponent<Textured>();
                
                renderer.AddRenderAction(textureComponent.Layer, () => {
                    RenderEntity(locationComponent, textureComponent);
                });
            }
        }
        
        private void RenderEntity(Location locationComponent, Textured textureComponent)
        {
            GL.Enable(EnableCap.Texture2D);
            GL.Color3(Color.White);
            
            var location = locationComponent.CurrentLocation;
            float x = location.X;
            float y = location.Y;
             
            // TODO: Logging
            if (textureComponent.CurrentAnimation == null)
            {
                return;
            }
            
            var animationName = textureComponent.CurrentAnimation;
            var animation = this.assetManager.Animations[animationName];
            var frame = animation.GetFrame(textureComponent.Frame);
            
            GL.BindTexture(TextureTarget.Texture2D, this.textures.GetTextureID(animation.Texture.FileName));
             
            var frameWidth = 1.0 / animation.Texture.HorizontalFrames;
            var frameHeight = 1.0 / animation.Texture.VerticalFrames;
            var startFrameX = frameWidth * frame.FrameX;
            var startFrameY = frameHeight * frame.FrameY;
            
            double xStart, xEnd;
            if (animation.InvertX)
            {
                xStart = startFrameX + frameWidth;
                xEnd = startFrameX;
            }
            else
            {
                xStart = startFrameX;
                xEnd = startFrameX + frameWidth;
            }
            
            float windowFrameHalfWidth = animation.Texture.FrameGameWidth / 2.0f;
            float windowFrameHalfHeight = animation.Texture.FrameGameHeight / 2.0f;
            
            GL.Begin(BeginMode.Quads);
            GL.TexCoord2(xStart, startFrameY + frameHeight);
            GL.Vertex2(x - windowFrameHalfWidth, y - windowFrameHalfHeight);
            GL.TexCoord2(xEnd, startFrameY + frameHeight);
            GL.Vertex2(x + windowFrameHalfWidth, y - windowFrameHalfHeight);
            GL.TexCoord2(xEnd, startFrameY);
            GL.Vertex2(x + windowFrameHalfWidth, y + windowFrameHalfHeight);
            GL.TexCoord2(xStart, startFrameY);
            GL.Vertex2(x - windowFrameHalfWidth, y + windowFrameHalfHeight);
            GL.End();
        }
    }
}

