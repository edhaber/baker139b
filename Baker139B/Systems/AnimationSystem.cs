using System;
using System.Linq;
using Baker139.CBES;
using Baker139B.Graphics;
using Baker139B.Components;
using OpenTK;
using Baker139B.Assets.IF;
using Baker139;
using Baker139B.Common;

namespace Baker139B.Systems
{
    public class AnimationSystem : IUpdateSystem
    {
        private const double FRAME_DURATION = 0.05; // S
        private const int ANIMATION_DIRECTIONS = 12;
        private readonly IAssetManager assetManager;
        
        public AnimationSystem(IAssetManager assetManager)
        {
            this.assetManager = assetManager;
        }
        
        public void EntityAdded(World world, Entity e)
        {
        }

        public void EntityRemoved(World world, Entity e)
        {
        }
        
        public void Update(World world, double ms)
        {
            var entities = from entity in world.FindEntities()
                where entity.HasComponent<Animated>()
                select entity;
            
            foreach (var entity in entities)
            {
                var location = entity.GetComponent<Location>();
                var movement = entity.GetComponent<Movement>();
                var weaponized = entity.GetComponent<Weaponized>();
                var animation = entity.GetComponent<Animated>();
                var texture = entity.GetComponent<Textured>();
                
                UpdateEntity(world, location, movement, weaponized, animation, texture, ms);
            }
        }
        
        private void UpdateEntity(World world,
                                  Location locationComponent, Movement movementComponent, Weaponized weaponizedComponent,
                                  Animated animationComponent, Textured textureComponent, double timeElapsed)
        {
            if (textureComponent.CurrentAnimation == null)
            {
                textureComponent.CurrentAnimation = "Marine.Default";
                textureComponent.Frame = 0;
                animationComponent.TimeOnFrame = 0;
            }
            
            var newAnimation = "Marine.Default";
            
            var location = locationComponent.CurrentLocation;
            int direction;
            
            if (movementComponent.HasDestination)
            {
                var destination = movementComponent.CurrentDestination;
                direction = GetDirection(location, destination, ANIMATION_DIRECTIONS);
                
                if (direction == 0)
                {
                    newAnimation = "Marine.Walk12";
                }
                else
                {
                    newAnimation = "Marine.Walk" + direction;
                }
            }
            
            if (weaponizedComponent.TargetEntityID.HasValue)
            {
                var targetEntityID = weaponizedComponent.TargetEntityID.Value;
                if (world.HasEntity(targetEntityID))
                {
                    var targetLocation = world.GetEntity(targetEntityID).GetComponent<Location>().CurrentLocation;
                    direction = GetDirection(location, targetLocation, ANIMATION_DIRECTIONS);
                    
                    if (direction == 0)
                    {
                        newAnimation = "Marine.Fire12";
                    }
                    else
                    {
                        newAnimation = "Marine.Fire" + direction;
                    }
                }
            }
            
            if (newAnimation != textureComponent.CurrentAnimation)
            {
                animationComponent.TimeOnFrame = 0;
                textureComponent.Frame = 0;
            }
            else
            {
                double newTimeOnFrame = animationComponent.TimeOnFrame + timeElapsed;
                var framesInAnimation = this.assetManager.Animations[newAnimation].FrameCount;
                textureComponent.Frame = (int)((textureComponent.Frame * FRAME_DURATION + newTimeOnFrame) / FRAME_DURATION) % framesInAnimation;
                animationComponent.TimeOnFrame = newTimeOnFrame % FRAME_DURATION;
            }
            textureComponent.CurrentAnimation = newAnimation;
        }
        
        /// <summary>
        /// find the direction that one would travel to get from `sourcePoint` 
        /// to `destinationPoint`. Given that a unit can travel in `directions` different
        /// directions, returns the numeric value for the direction it will travel. For example,
        /// given a directions of 4, a due-north direction will provide 0, a due-east will provide
        /// 1, etc. 
        /// </summary>
        // TODO: Put this in a utility class and test the shit out of it.
        private int GetDirection(WorldCoord sourcePoint, WorldCoord destinationPoint, int directions)
        {
            var vector = destinationPoint - sourcePoint;
            var degrees = 90d - Math.Atan2(vector.Y, vector.X) / (Math.PI / 180d);
            degrees = ((degrees % 360) + 360) % 360; // WTF is this hack? Why can't .net % handle negatives?
            var directionAngleWidth = 360 / directions;
            var directionAngleOffset = directionAngleWidth / 2;
            return (int)((degrees + directionAngleOffset) / directionAngleWidth);
        }
    }
}

