using System;

namespace Baker139B
{
    public enum GraphicsLayer : int
    {
        Terrain = 0,
        UnitsUI,
        Units,
        UI,
    }
}

