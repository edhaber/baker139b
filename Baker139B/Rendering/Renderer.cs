using System;
using System.Linq;
using System.Collections.Generic;

namespace Baker139B.Rendering
{
    public class Renderer
    {
        private readonly IDictionary<int, IList<Action>> actions;
        
        public Renderer()
        {
            this.actions = new Dictionary<int, IList<Action>>();
        }
        
        public void AddRenderAction(GraphicsLayer layer, Action action)
        {
            var key = (int)layer;
            if (!this.actions.ContainsKey(key))
            {
                this.actions[key] = new List<Action>();
            }
            
            this.actions[key].Add(action);
        }
        
        public void Render()
        {
            var actionGroups = from keyValuePair in this.actions
                orderby keyValuePair.Key
                select keyValuePair.Value;
            
            foreach (var actionGroup in actionGroups)
            {
                foreach (var action in actionGroup)
                {
                    action.Invoke();
                }
            }
        }
    }
}

