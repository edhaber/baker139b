using System;
using OpenTK.Graphics.OpenGL;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;

namespace Baker139B
{
    public class TextureCollection
    {
        private readonly IDictionary<string, uint> textureAliases;
        
        public TextureCollection ()
        {
            this.textureAliases = new Dictionary<string, uint>();
        }
        
        public void LoadTexture(string fileName)
        {
            Bitmap bitmap = (Bitmap)Bitmap.FromFile(fileName);
            
            //get the data out of the bitmap
            BitmapData data = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height),
                ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            
            //generate one texture
            GL.Enable(EnableCap.Texture2D);
            uint textureID;
            GL.GenTextures(1, out textureID);
            
            //tell OpenGL that this is a 2D texture
            GL.BindTexture(TextureTarget.Texture2D, textureID);
            
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, data.Width, data.Height, 0, 
                OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, data.Scan0);  
            
            //free the bitmap data (we dont need it anymore because it has been passed to the OpenGL driver)
            bitmap.UnlockBits(data);
            
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            
            
            this.textureAliases[fileName] = textureID;
        }

        public uint GetTextureID (string alias)
        {
            return this.textureAliases[alias];
        }
    }
}

