using System;

namespace Baker139.CBES
{
    public class CBESException : Exception
    {
        public CBESException(string message) : base(message) {}
    }
}

