using System;
using System.Collections.Generic;

namespace Baker139.CBES
{
    public delegate void EntityEvent(World world, Entity entity);
    
    public class World
    {
        private int nextId = 0;
        private IDictionary<int, Entity> entities;
        
        public World()
        {
            this.entities = new Dictionary<int, Entity>();
        }

        public Entity BuildEntity(params IComponent[] components)
        {
            var id = this.nextId++;
            var entity = new Entity(id, components);
            this.entities[id] = entity;
            if (null != this.EntityAdded)
            {
                this.EntityAdded(this, entity);
            }
            return entity;
        }

        public void RemoveEntity(Entity entityToRemove)
        {
            if (!this.entities.ContainsKey(entityToRemove.ID))
            {
                var msg = String.Format("Entity '{0}' was not found, and thus cannot be removed", entityToRemove.ID);
                throw new CBESException(msg);
            }
            
            this.entities.Remove(entityToRemove.ID);
            if (null != this.EntityRemoved)
            {
                this.EntityRemoved(this, entityToRemove);
            }
        }

        public bool HasEntity(int entityID)
        {
            return this.entities.ContainsKey(entityID);
        }

        public Entity GetEntity(int entityID)
        {
            if (!this.entities.ContainsKey(entityID))
            {
                var msg = String.Format("Entity '{0}' was not found", entityID);
                throw new CBESException(msg);
            }
            return this.entities[entityID];
        }

        public IEnumerable<Entity> FindEntities()
        {
            foreach (var keyValuePair in this.entities)
            {
                yield return keyValuePair.Value;
            }
        }
        
        public event EntityEvent EntityAdded;
        public event EntityEvent EntityRemoved;
    }
}

