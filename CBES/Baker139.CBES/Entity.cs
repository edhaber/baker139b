using System;
using System.Collections.Generic;

namespace Baker139.CBES
{
    public class Entity
    {
        private readonly IDictionary<Type, IComponent> components;
        private int id;
        
        public Entity(int id, IComponent[] components)
        {
            this.id = id;
            
            this.components = new Dictionary<Type, IComponent>(components.Length);
            foreach (IComponent component in components)
            {
                var type = component.GetType();
                if (this.components.ContainsKey(type))
                {
                    var msg = "The component '{0}' was included more than once in the list of components for this entity";
                    throw new CBESException(String.Format(msg, type.ToString()));
                }
                this.components[component.GetType()] = component;
            }
            
        }

        public int ID
        {
            get
            {
                return this.id;
            }
        }
        
        public bool HasComponent<T>()
        {
            var type = typeof(T);
            return this.components.ContainsKey(type);
        }

        public T GetComponent<T>()
            where T : IComponent
        {
            var type = typeof(T);
            if (!this.components.ContainsKey(type))
            {
                var msg = String.Format("The entity does not contain the component '{0}'", type.ToString());
                throw new CBESException(msg);
            }
            return (T)this.components[typeof(T)];
        }
    }
}

