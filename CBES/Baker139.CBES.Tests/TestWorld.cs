using System;
using System.Linq;
using NUnit.Framework;
using System.Collections.Generic;

namespace Baker139.CBES.Tests
{
    [TestFixture]
    public class TestWorld
    {
        [Test]
        public void TestWorldGeneratesDifferentIDs()
        {
            var world = new World();
            var e1 = world.BuildEntity();
            var e2 = world.BuildEntity();
            
            Assert.AreNotEqual(e1.ID, e2.ID);
        }
        
        [Test]
        public void TestCanRetrieveEntityByID()
        {
            var world = new World();
            var e1 = world.BuildEntity();
            var e2 = world.GetEntity(e1.ID);
            
            Assert.AreSame(e1, e2);
        }
        
        [Test]
        public void TestHasEntityFalseForNonExistantEntity()
        {
            var world = new World();
            Assert.IsFalse(world.HasEntity(1));
        }
        
        [Test]
        public void TestHasEntityTrueForExistantEntity()
        {
            var world = new World();
            var entity = world.BuildEntity();
            Assert.IsTrue(world.HasEntity(entity.ID));
        }
        
        [Test]
        public void TestHasEntityFalseForRemovedEntity()
        {
            var world = new World();
            var entity = world.BuildEntity();
            var id = entity.ID;
            world.RemoveEntity(entity);
            Assert.IsFalse(world.HasEntity(id));
        }
        
        [Test]
        public void TestRetrievingUnknownEntityThrowsException()
        {
            var world = new World();
            
            var ex = Assert.Throws<CBESException>(() => world.GetEntity(55));
            Assert.IsTrue(ex.Message.ToLower().Contains("not found"),
                          String.Format("Error message '{0}' is not descriptive enough", ex.Message));
            
        }
        
        [Test]
        public void TestFindEntityThroughQuery()
        {
            var world = new World();
            world.BuildEntity(
                new FakeComponent() { X = 1 }
            );
            
            var e2 = world.BuildEntity(
                new FakeComponent() { X = 2 }
            );
            
            var query =
                from entity in world.FindEntities()
                where entity.HasComponent<FakeComponent>() &&
                      entity.GetComponent<FakeComponent>().X == 2
                select entity;

            var results = query.ToList();
            Assert.AreEqual(1, results.Count());
            Assert.AreSame(e2, results[0]);
        }
        
        [Test]
        public void TestAddEntityEvent()
        {
            var world = new World();
            
            int eventCalledCount = 0;
            World worldSentToEvent = null;
            Entity entitySentToEvent = null;
                
            world.EntityAdded += (World w, Entity e) => {
                eventCalledCount += 1;
                worldSentToEvent = world;
                entitySentToEvent = e;
            };
            
            var resultingEntity = world.BuildEntity();
            
            Assert.AreEqual(1, eventCalledCount);
            Assert.AreEqual(world, worldSentToEvent);
            Assert.AreEqual(resultingEntity, entitySentToEvent);
        }
        
        [Test]
        public void TestCanAddEntityWithoutAddEventSubscriptions()
        {
            var world = new World();
            world.BuildEntity();
        }
        
        [Test]
        public void TestRemoveEntityEvent()
        {
            var world = new World();
            
            var resultingEntity = world.BuildEntity();
            
            int eventCalledCount = 0;
            World worldSentToEvent = null;
            Entity entitySentToEvent = null;
                
            world.EntityRemoved += (World w, Entity e) => {
                eventCalledCount += 1;
                worldSentToEvent = world;
                entitySentToEvent = e;
            };
            
            world.RemoveEntity(resultingEntity);
            
            Assert.AreEqual(1, eventCalledCount);
            Assert.AreEqual(world, worldSentToEvent);
            Assert.AreEqual(resultingEntity, entitySentToEvent);
        }
        
        [Test]
        public void TestCanCallRemoveEntityWithoutSubscribers()
        {
            var world = new World();
            var entity = world.BuildEntity();
            world.RemoveEntity(entity);
        }
        
        [Test]
        public void TestCannotFindEntityByIDAfterRemoved()
        {
            var world = new World();
            var entity = world.BuildEntity();
            var entityID = entity.ID;
            
            world.RemoveEntity(entity);
            
            var ex = Assert.Throws<CBESException>(() => world.GetEntity(entityID));
            Assert.IsTrue(ex.Message.ToLower().Contains("not found"),
                          String.Format("Error message '{0}' is not descriptive enough", ex.Message));
            
        }
        
        [Test]
        public void TestCannotFindEntityThroughQueryAfterRemoved()
        {
            var world = new World();
            world.BuildEntity(
                new FakeComponent() { X = 1 }
            );
            
            var e2 = world.BuildEntity(
                new FakeComponent() { X = 2 }
            );
            
            world.RemoveEntity(e2);
            
            var query =
                from entity in world.FindEntities()
                where entity.HasComponent<FakeComponent>() &&
                      entity.GetComponent<FakeComponent>().X == 2
                select entity;

            var results = query.ToList();
            Assert.AreEqual(0, results.Count());
        }
        
        [Test]
        public void TestExceptionWhenTryingToRemoveEntityNotInWorld()
        {
            var world = new World();
            var e = world.BuildEntity(
                new FakeComponent() { X = 1 }
            );
            
            world.RemoveEntity(e);
            
            var ex = Assert.Throws<CBESException>(() => world.RemoveEntity(e));
            
            Assert.IsTrue(ex.Message.ToLower().Contains("not found"),
                          String.Format("Error message '{0}' is not descriptive enough", ex.Message));
        }
    }
}

