using System;
using NUnit.Framework;
using Baker139.CBES;

namespace Baker139.CBES.Tests
{
    [TestFixture]
    public class TestEntity
    {
        [Test]
        public void TestCanCreateEntityWithID()
        {
            var entity = new Entity(5, new IComponent[] {});
            Assert.AreEqual(5, entity.ID);
        }
        
        [Test]
        public void TestCanAddComponent()
        {
            var entity = new Entity(1, new IComponent[] {
                new FakeComponent() { X = 5 }
            });
            Assert.AreEqual(5, entity.GetComponent<FakeComponent>().X, "Should be able to use component");
        }
        
        [Test]
        public void ErrorWhenRetrievingComponent()
        {
            var entity = new Entity(1, new IComponent[] {});
            var ex = Assert.Throws<CBESException>(() => entity.GetComponent<FakeComponent>());
            Assert.IsTrue(ex.Message.ToLower().Contains("entity does not contain the component"),
                          String.Format("Error message '{0}' is not informative enough", ex.Message));
            Assert.IsTrue(ex.Message.Contains("FakeComponent"),
                          String.Format("Error message '{0}' is not informative enough", ex.Message));
        }
        
        [Test]
        public void ErrorAddingDuplicateComponents()
        {
            var ex = Assert.Throws<CBESException>(() => {
                new Entity(1, new IComponent[] {
                    new FakeComponent(),
                    new FakeComponent()
                });
            });
            
            Assert.IsTrue(ex.Message.ToLower().Contains("was included more than once"),
                          String.Format("Error message '{0}' not informative enough", ex.Message));
            Assert.IsTrue(ex.Message.Contains("FakeComponent"),
                          String.Format("Error message '{0}' not informative enough", ex.Message));
            
        }
        
        [Test]
        public void TestHasComponentIsTrue()
        {
            var entity = new Entity(1, new IComponent[] {
                new FakeComponent()
            });
            
            Assert.IsTrue(entity.HasComponent<FakeComponent>());
        }
        
        [Test]
        public void TestHasComponentIsFalse()
        {
            var entity = new Entity(1, new IComponent[] {
                new FakeComponent(),
            });
            
            Assert.IsFalse(entity.HasComponent<AnotherFakeComponent>());
        }
    }
}

