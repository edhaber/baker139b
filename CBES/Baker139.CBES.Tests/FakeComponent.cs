using System;

namespace Baker139.CBES.Tests
{
    public class FakeComponent : IComponent
    {
        private int x;
        
        public FakeComponent()
        {
        }
        
        public int X
        {
            get { return this.x; }
            set { this.x = value; }
        }
    }
}

