using System;
using System.IO;
using NUnit.Framework;
using Baker139B.Assets.Model;
using Baker139B.Assets.IF;
using System.Collections.Generic;
using System.Linq;

namespace Baker139B.Assets.Tests
{
    /// <summary>
    /// Asset manager tests.
    /// These test that the asset manager can load an asset file correctly.
    /// </summary>
    [TestFixture]
    public class AssetManagerTests
    {
        /// <summary>
        /// Loads an AssetManager with data from files filled with asset data "data".
        /// For each data, one file will be created and the results loaded.
        /// The files will then be automatically deleted.
        /// The purpose of this method is to encapsulate the process of creating test
        /// data for the manager to load.
        /// </summary>
        /// <returns>
        /// An asset manager already loaded with the data specified.
        /// </returns>
        /// <param name='data'>
        /// A string of asset data.
        /// </param>
        private IAssetManager RunLoad(params string[] dataToLoad)
        {
            var manager = new AssetManager();
            var tempDirectory = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
            
            try
            {
                Directory.CreateDirectory(tempDirectory);
            
                foreach (string data in dataToLoad)
                {
                    var fileName = Path.Combine(tempDirectory, "TestFile.yaml");
                    File.WriteAllText(fileName, data);
                    manager.LoadFile(fileName);
                }
            }
            finally
            {
                try
                {
                    Directory.Delete(tempDirectory, true);
                }
                catch (IOException)
                {
                    // Don't care
                }
            }
            
            return manager;
        }
        
        [Test]
        public void TestAnimationGetsFileNameCorrect()
        {
            var manager = RunLoad(
                @"{
                    'Sprite' : {
                        'FileName' : 'Test.png',
                        'FrameWidth' : 32,
                        'FrameHeight' : 64,
                        'FrameGameWidth' : 8,
                        'FrameGameHeight' : 8,
                        'HorizontalFrames' : 2,
                        'VerticalFrames' : 1,
                        'Animations' : [
                            {
                                'Name' : 'Dog.Bark',
                                'Frames' : [ [0,0], [1,0] ],
                            }
                        ],
                    }
                }");
            
            var animation = manager.Animations["Dog.Bark"];
            Assert.AreEqual("Test.png", animation.Texture.FileName);
        }
        
        [Test]
        public void TestFindTextureFrameDimensions()
        {
            var manager = RunLoad(
                @"{
                    'Sprite' : {
                        'FileName' : 'Test.png',
                        'FrameWidth' : 32,
                        'FrameHeight' : 64,
                        'FrameGameWidth' : 8,
                        'FrameGameHeight' : 8,
                        'HorizontalFrames' : 2,
                        'VerticalFrames' : 1,
                        'Animations' : [
                            {
                                'Name' : 'Dog.Bark',
                                'Frames' : [ [0,0], [1,0] ],
                            }
                        ],
                    }
                }");
            
            var animation = manager.Animations["Dog.Bark"];
            
            Assert.AreEqual(2, animation.Texture.HorizontalFrames);
            Assert.AreEqual(1, animation.Texture.VerticalFrames);
        }
        
        [Test]
        public void TestAnimationFrameInfo()
        {
            var manager = RunLoad(
                @"{
                    'Sprite' : {
                        'FileName' : 'Test.png',
                        'FrameWidth' : 32,
                        'FrameHeight' : 64,
                        'FrameGameWidth' : 8,
                        'FrameGameHeight' : 8,
                        'HorizontalFrames' : 5,
                        'VerticalFrames' : 5,
                        'Animations' : [
                            {
                                'Name' : 'Dog.Bark',
                                'Frames' : [ [1,2], [3,4] ],
                            }
                        ],
                    }
                }");
            
            var animation = manager.Animations["Dog.Bark"];
            
            Assert.AreEqual(2, animation.FrameCount);
            Assert.AreEqual(1, animation.GetFrame(0).FrameX);
            Assert.AreEqual(2, animation.GetFrame(0).FrameY);
            Assert.AreEqual(3, animation.GetFrame(1).FrameX);
            Assert.AreEqual(4, animation.GetFrame(1).FrameY);
        }
        
        [Test]
        public void TestInvalidAssetWhenFileNameMissing()
        {
            var data = @"
                {
                    'Sprite' : {
                        'FrameWidth' : 32,
                        'FrameHeight' : 64,
                        'FrameGameWidth' : 8,
                        'FrameGameHeight' : 8,
                        'HorizontalFrames' : 4,
                        'VerticalFrames' : 4,
                        'Animations' : [
                            {
                                'Name' : 'Dog.Bark',
                                'Frames' : [ [1,2], [3,4] ],
                            }
                        ],
                    }
                }";
            Assert.Throws<AssetLoadException>(() => RunLoad(data));
        }
        
        [Test]
        public void TestInvalidAssetWhenHorizontalFramesMissing()
        {
            var data = @"
                {
                    'Sprite' : {
                        'FileName' : 'Test.png',
                        'FrameWidth' : 32,
                        'FrameHeight' : 64,
                        'FrameGameWidth' : 8,
                        'FrameGameHeight' : 8,
                        'VerticalFrames' : 4,
                        'Animations' : [
                            {
                                'Name' : 'Dog.Bark',
                                'Frames' : [ [1,2], [3,4] ],
                            }
                        ],
                    }
                }";
            
            Assert.Throws<AssetLoadException>(() => RunLoad(data));
        }
        
        [Test]
        public void TestInvalidAssetWhenHorizontalFrameMissing()
        {
            var data = @"
                {
                    'Sprite' : {
                        'FileName' : 'Test.png',
                        'FrameWidth' : 32,
                        'FrameHeight' : 64,
                        'FrameGameWidth' : 8,
                        'FrameGameHeight' : 8,
                        'HorizontalFrames' : 4,
                        'Animations' : [
                            {
                                'Name' : 'Dog.Bark',
                                'Frames' : [ [1,2], [3,4] ],
                            }
                        ],
                    }
                }";
            Assert.Throws<AssetLoadException>(() => RunLoad(data));
        }
        
        [Test]
        public void TestInvalidAssetWhenNoAnimations()
        {
            var data = @"
                {
                    'Sprite' : {
                        'FileName' : 'Test.png',
                        'FrameWidth' : 32,
                        'FrameHeight' : 64,
                        'FrameGameWidth' : 8,
                        'FrameGameHeight' : 8,
                        'HorizontalFrames' : 4,
                        'VerticalFrames' : 4,
                    }
                }";
            var ex = Assert.Throws<AssetLoadException>(() => RunLoad(data));
            Assert.IsTrue(ex.ErrorMessage.ToLower().Contains("animations"));
        }
        
        [Test]
        public void TestInvalidAssetWithMissingAnimationName()
        {
            var data = @"
                {
                    'Sprite' : {
                        'FileName' : 'Test.png',
                        'FrameWidth' : 32,
                        'FrameHeight' : 64,
                        'FrameGameWidth' : 8,
                        'FrameGameHeight' : 8,
                        'HorizontalFrames' : 2,
                        'VerticalFrames' : 2,
                        'Animations' : [
                            {
                                'Frames' : [ [0,0] ],
                            }
                        ],
                    }
                }";
            Assert.Throws<AssetLoadException>(() => RunLoad(data));
        }
        
        [Test]
        public void TestInvalidAssetWithOutOfBoundsFrame()
        {
            var data = @"
                {
                    'Sprite' : {
                        'FileName' : 'Test.png',
                        'FrameWidth' : 32,
                        'FrameHeight' : 64,
                        'FrameGameWidth' : 8,
                        'FrameGameHeight' : 8,
                        'HorizontalFrames' : 2,
                        'VerticalFrames' : 2,
                        'Animations' : [
                            {
                                'Name' : 'Dog.Bark',
                                'Frames' : [ [3,3] ],
                            }
                        ],
                    }
                }";
            Assert.Throws<AssetLoadException>(() => RunLoad(data));
        }
        
        [Test]
        public void TestInvalidAssetWithInvalidFrameArray()
        {
            var data = @"
                {
                    'Sprite' : {
                        'FileName' : 'Test.png',
                        'FrameWidth' : 32,
                        'FrameHeight' : 64,
                        'FrameGameWidth' : 8,
                        'FrameGameHeight' : 8,
                        'HorizontalFrames' : 2,
                        'VerticalFrames' : 2,
                        'Animations' : [
                            {
                                'Name' : 'Dog.Bark',
                                'Frames' : [0,0],
                            }
                        ],
                    }
                }";
            Assert.Throws<AssetLoadException>(() => RunLoad(data));
        }
        
        [Test]
        public void TestRetrieveRequiredTextures()
        {
            var data1 = @"
                {
                    'Sprite' : {
                        'FileName' : 'Test1.png',
                        'FrameWidth' : 32,
                        'FrameHeight' : 64,
                        'FrameGameWidth' : 8,
                        'FrameGameHeight' : 8,
                        'HorizontalFrames' : 2,
                        'VerticalFrames' : 2,
                        'Animations' : [
                            {
                                'Name' : 'Dog.Bark',
                                'Frames' : [ [0,0] ],
                            }
                        ],
                    }
                }";
            
            var data2 = @"
                {
                    'Sprite' : {
                        'FileName' : 'Test2.png',
                        'FrameWidth' : 32,
                        'FrameHeight' : 64,
                        'FrameGameWidth' : 8,
                        'FrameGameHeight' : 8,
                        'HorizontalFrames' : 2,
                        'VerticalFrames' : 2,
                        'Animations' : [
                            {
                                'Name' : 'Cat.Meow',
                                'Frames' : [ [0,0] ],
                            }
                        ],
                    }
                }";
            
            var manager = RunLoad(data1, data2);
            
            var results = new List<string>(manager.Animations.RequiredTextures);
            Assert.AreEqual(2, results.Count);
            Assert.IsTrue(results.Contains("Test1.png"));
            Assert.IsTrue(results.Contains("Test2.png"));
        }
        
        [Test]
        public void TestNoDuplicationInRequiredTexturesResults()
        {
            var data1 = @"
                {
                    'Sprite' : {
                        'FileName' : 'Test.png',
                        'FrameWidth' : 32,
                        'FrameHeight' : 64,
                        'FrameGameWidth' : 8,
                        'FrameGameHeight' : 8,
                        'HorizontalFrames' : 2,
                        'VerticalFrames' : 2,
                        'Animations' : [
                            {
                                'Name' : 'Dog.Bark',
                                'Frames' : [ [0,0] ],
                            }
                        ],
                    }
                }";
            
            var data2 = @"
                {
                    'Sprite' : {
                        'FileName' : 'Test.png',
                        'FrameWidth' : 32,
                        'FrameHeight' : 64,
                        'FrameGameWidth' : 8,
                        'FrameGameHeight' : 8,
                        'HorizontalFrames' : 2,
                        'VerticalFrames' : 2,
                        'Animations' : [
                            {
                                'Name' : 'Cat.Meow',
                                'Frames' : [ [0,0] ],
                            }
                        ],
                    }
                }";
            
            var manager = RunLoad(data1, data2);
            Assert.AreEqual(1, manager.Animations.RequiredTextures.Count());
        }
        
        [Test]
        public void TestDuplicateAnimationNameDetection()
        {
            var data1 = @"
                {
                    'Sprite' : {
                        'FileName' : 'Test1.png',
                        'FrameWidth' : 32,
                        'FrameHeight' : 64,
                        'FrameGameWidth' : 8,
                        'FrameGameHeight' : 8,
                        'HorizontalFrames' : 2,
                        'VerticalFrames' : 2,
                        'Animations' : [
                            {
                                'Name' : 'Dog.Bark',
                                'Frames' : [ [0,0] ],
                            }
                        ],
                    }
                }";
            
            var data2 = @"
                {
                    'Sprite' : {
                        'FileName' : 'Test2.png',
                        'FrameWidth' : 32,
                        'FrameHeight' : 64,
                        'FrameGameWidth' : 8,
                        'FrameGameHeight' : 8,
                        'HorizontalFrames' : 2,
                        'VerticalFrames' : 2,
                        'Animations' : [
                            {
                                'Name' : 'Dog.Bark',
                                'Frames' : [ [0,0] ],
                            }
                        ],
                    }
                }";
            
            var ex = Assert.Throws<AssetLoadException>(() => RunLoad(data1, data2));
            Assert.IsTrue(ex.ErrorMessage.ToLower().Contains("previously declared"));
        }
        
        [Test]
        public void TestInvertXDefaultsToFalse()
        {
            var data = @"
                {
                    'Sprite' : {
                        'FileName' : 'Test.png',
                        'FrameWidth' : 32,
                        'FrameHeight' : 64,
                        'FrameGameWidth' : 8,
                        'FrameGameHeight' : 8,
                        'HorizontalFrames' : 2,
                        'VerticalFrames' : 2,
                        'Animations' : [
                            {
                                'Name' : 'Dog.Bark',
                                'Frames' : [ [0,0] ],
                            }
                        ],
                    }
                }";
            
            var manager = RunLoad(data);
            Assert.AreEqual(false, manager.Animations["Dog.Bark"].InvertX);
        }
        
        [Test]
        public void TestCanSpecifyInvertX()
        {
            var data = @"
                {
                    'Sprite' : {
                        'FileName' : 'Test.png',
                        'FrameWidth' : 32,
                        'FrameHeight' : 64,
                        'FrameGameWidth' : 8,
                        'FrameGameHeight' : 8,
                        'HorizontalFrames' : 2,
                        'VerticalFrames' : 2,
                        'Animations' : [
                            {
                                'Name' : 'Dog.Bark',
                                'InvertX' : true,
                                'Frames' : [ [0,0] ],
                            }
                        ],
                    }
                }";
            
            var manager = RunLoad(data);
            Assert.AreEqual(true, manager.Animations["Dog.Bark"].InvertX);
        }
        
        [Test]
        public void CanSpecifyFrameSize()
        {
            var data = @"
                {
                    'Sprite' : {
                        'FileName' : 'Test.png',
                        'FrameWidth' : 32,
                        'FrameHeight' : 64,
                        'FrameGameWidth' : 8,
                        'FrameGameHeight' : 8,
                        'HorizontalFrames' : 2,
                        'VerticalFrames' : 2,
                        'Animations' : [
                            {
                                'Name' : 'Dog.Bark',
                                'InvertX' : true,
                                'Frames' : [ [0,0] ],
                            }
                        ],
                    }
                }";
            
            var manager = RunLoad(data);
            Assert.AreEqual(32, manager.Animations["Dog.Bark"].Texture.FrameWidth);
            Assert.AreEqual(64, manager.Animations["Dog.Bark"].Texture.FrameHeight);
        }
        
        [Test]
        public void TestErrorOccursIfFrameWidthIsMissing()
        {
            var data = @"
                {
                    'Sprite' : {
                        'FileName' : 'Test.png',
                        'FrameHeight' : 32,
                        'FrameGameWidth' : 8,
                        'FrameGameHeight' : 8,
                        'HorizontalFrames' : 2,
                        'VerticalFrames' : 2,
                        'Animations' : [
                            {
                                'Name' : 'Dog.Bark',
                                'InvertX' : true,
                                'Frames' : [ [0,0] ],
                            }
                        ],
                    }
                }";
            
            var ex = Assert.Throws<AssetLoadException>(() => RunLoad(data));
            Assert.IsTrue(ex.ErrorMessage.Contains("FrameWidth"));
        }
        
        [Test]
        public void TestErrorOccursIfFrameHeightIsMissing()
        {
            var data = @"
                {
                    'Sprite' : {
                        'FileName' : 'Test.png',
                        'FrameWidth' : 32,
                        'FrameGameWidth' : 8,
                        'FrameGameHeight' : 8,
                        'HorizontalFrames' : 2,
                        'VerticalFrames' : 2,
                        'Animations' : [
                            {
                                'Name' : 'Dog.Bark',
                                'InvertX' : true,
                                'Frames' : [ [0,0] ],
                            }
                        ],
                    }
                }";
            
            var ex = Assert.Throws<AssetLoadException>(() => RunLoad(data));
            Assert.IsTrue(ex.ErrorMessage.Contains("FrameHeight"));
        }
        
        [Test]
        public void TestErrorOccursIfFrameGameWidthIsMissing()
        {
            var data = @"
                {
                    'Sprite' : {
                        'FileName' : 'Test.png',
                        'FrameWidth' : 32,
                        'FrameHeight' : 32,
                        'FrameGameHeight' : 8,
                        'HorizontalFrames' : 2,
                        'VerticalFrames' : 2,
                        'Animations' : [
                            {
                                'Name' : 'Dog.Bark',
                                'InvertX' : true,
                                'Frames' : [ [0,0] ],
                            }
                        ],
                    }
                }";
            
            var ex = Assert.Throws<AssetLoadException>(() => RunLoad(data));
            Assert.IsTrue(ex.ErrorMessage.Contains("FrameGameWidth"));
        }
        
        [Test]
        public void TestErrorOccursIfFrameGameHeightIsMissing()
        {
            var data = @"
                {
                    'Sprite' : {
                        'FileName' : 'Test.png',
                        'FrameWidth' : 32,
                        'FrameHeight' : 32,
                        'FrameGameWidth': 8,
                        'HorizontalFrames' : 2,
                        'VerticalFrames' : 2,
                        'Animations' : [
                            {
                                'Name' : 'Dog.Bark',
                                'InvertX' : true,
                                'Frames' : [ [0,0] ],
                            }
                        ],
                    }
                }";
            
            var ex = Assert.Throws<AssetLoadException>(() => RunLoad(data));
            Assert.IsTrue(ex.ErrorMessage.Contains("FrameGameHeight"));
        }
    }
}

