using System;

namespace Baker139B.Assets.IF
{
    public interface IAnimationAsset
    {
        ITextureAsset Texture { get; }
        int FrameCount { get; }
        IAnimationFrameAsset GetFrame(int count);
        bool InvertX { get; }
    }
}

