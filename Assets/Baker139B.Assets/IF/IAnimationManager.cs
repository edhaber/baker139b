using System;
using System.Collections.Generic;

namespace Baker139B.Assets.IF
{
    public interface IAnimationManager
    {
        IEnumerable<string> RequiredTextures { get; }
        IAnimationAsset this[string alias] { get; }
    }
}

