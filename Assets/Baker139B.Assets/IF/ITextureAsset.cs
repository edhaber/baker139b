using System;

namespace Baker139B.Assets.IF
{
    public interface ITextureAsset
    {
        string FileName { get; }
        int FrameWidth { get; }
        int FrameHeight { get; }
        int FrameGameWidth { get; }
        int FrameGameHeight { get; }
        int HorizontalFrames { get; }
        int VerticalFrames { get; }
    }
}

