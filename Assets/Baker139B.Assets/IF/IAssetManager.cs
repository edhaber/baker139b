using System;

namespace Baker139B.Assets.IF
{
    public interface IAssetManager
    {
        IAnimationManager Animations { get; }
    }
}

