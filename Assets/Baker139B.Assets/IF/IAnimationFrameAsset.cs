using System;

namespace Baker139B.Assets.IF
{
    public interface IAnimationFrameAsset
    {
        int FrameX { get; }
        int FrameY { get; }
    }
}

