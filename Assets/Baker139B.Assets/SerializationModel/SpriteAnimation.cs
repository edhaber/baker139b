using System;
using System.Collections.Generic;

namespace Baker139B.Assets.SerializationModel
{
    public class SpriteAnimation
    {
        public string Name { get; set; }
        public ICollection<int[]> Frames { get; set; }
        public bool InvertX { get; set; }
    }
}

