using System;

namespace Baker139B.Assets.SerializationModel
{
    public class Asset
    {
        public Sprite Sprite { get; set; }
    }
}

