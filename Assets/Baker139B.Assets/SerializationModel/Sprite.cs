using System;
using System.Collections.Generic;

namespace Baker139B.Assets.SerializationModel
{
    public class Sprite
    {
        public string FileName { get; set; }
        public int FrameWidth { get; set; }
        public int FrameHeight { get; set; }
        public int FrameGameWidth { get; set; }
        public int FrameGameHeight { get; set; }
        public int VerticalFrames { get; set; }
        public int HorizontalFrames { get; set; }
        public SpriteAnimation[] Animations { get; set; }
    }
}

