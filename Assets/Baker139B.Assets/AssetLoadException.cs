using System;

namespace Baker139B.Assets
{
    public class AssetLoadException : Exception
    {
        private readonly string sourceFileName;
        private readonly string errorMessage;
        
        public AssetLoadException(string sourceFileName, string errorMessage)
        {
            this.sourceFileName = sourceFileName;
            this.errorMessage = errorMessage;
        }
        
        public AssetLoadException(string sourceFileName, Exception exceptionRaised) : base("", exceptionRaised) // TODO: HACK
        {
            this.errorMessage = String.Format("The file format is invalid. Serialization error: {0}", exceptionRaised.Message);
        }
        
        public override string Message
        {
            get
            {
                return String.Format("An error occurred while loading asset file '{0}'. {1}", this.sourceFileName, this.errorMessage);
            }
        }
        
        public string SourceFileName
        {
            get { return this.sourceFileName; }
        }
        
        public string ErrorMessage
        {
            get { return this.errorMessage; }
        }
    }
}

