using System;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Baker139B.Assets.SerializationModel;
using Baker139B.Assets.IF;
using System.Collections.Generic;

namespace Baker139B.Assets.Model
{
    public class AssetManager : IAssetManager
    {
        private readonly AnimationManager animations;
        
        public AssetManager()
        {
            this.animations = new AnimationManager();
        }
        
        public void LoadFile(string fileName)
        {
            var serializer = new JsonSerializer();
            
            using (var reader = new StreamReader(fileName))
            {
                using (var jsonReader = new JsonTextReader(reader))
                {
                    Asset asset;
                    try
                    {
                        asset = serializer.Deserialize<Asset>(jsonReader);
                    }
                    catch (JsonSerializationException ex)
                    {
                        throw new AssetLoadException(fileName, ex);
                    }
                    if (asset.Sprite != null)
                    {
                        var sprite = asset.Sprite;
                        ValidateSprite(fileName, sprite);
                        
                        var textureAsset = new TextureAsset(sprite.FileName,
                                                            sprite.FrameWidth, sprite.FrameHeight,
                                                            sprite.FrameGameWidth, sprite.FrameGameHeight,
                                                            sprite.HorizontalFrames, sprite.VerticalFrames);
                        foreach (var animation in sprite.Animations)
                        {
                            if (this.animations.Contains(animation.Name))
                            {
                                var msg = String.Format("The animation '{0}' has already been previously declared", animation.Name);
                                throw new AssetLoadException(fileName, msg);
                            }
                            
                            var frames = new List<AnimationFrameAsset>();
                            foreach (var frame in animation.Frames)
                            {
                                frames.Add(new AnimationFrameAsset(frame[0], frame[1]));
                            }
                            var animationAsset = new AnimationAsset(textureAsset, frames, animation.InvertX);
                            this.animations.Add(animation.Name, animationAsset);
                        }
                    }
                }
            }
        }

        private void ValidateSprite (string fileName, Sprite sprite)
        {
            if (String.IsNullOrEmpty(sprite.FileName))
            {
                throw new AssetLoadException(fileName, "Sprite element must contain a 'FileName' parameter");
            }
            
            if (sprite.FrameWidth == 0)
            {
                throw new AssetLoadException(fileName, "Sprite element has an invalid or missing 'FrameWidth' parameter");
            }
            
            if (sprite.FrameHeight == 0)
            {
                throw new AssetLoadException(fileName, "Sprite element has an invalid or missing 'FrameHeight' parameter");
            }

            if (sprite.FrameGameWidth == 0)
            {
                throw new AssetLoadException(fileName, "Sprite element has an invalid or missing 'FrameGameWidth' parameter");
            }
            
            if (sprite.FrameGameHeight == 0)
            {
                throw new AssetLoadException(fileName, "Sprite element has an invalid or missing 'FrameGameHeight' parameter");
            }
            
            if (sprite.HorizontalFrames == 0)
            {
                throw new AssetLoadException(fileName, "Sprite element has an invalid or missing 'HorizontalFrames' parameter");
            }
            
            if (sprite.VerticalFrames == 0)
            {
                throw new AssetLoadException(fileName, "Sprite element has an invalid or missing 'VerticalFrames' parameter");
            }
            
            if (null == sprite.Animations)
            {
                throw new AssetLoadException(fileName, "Sprite element has no animations defined");
            }
            
            foreach (var animation in sprite.Animations)
            {
                if (String.IsNullOrEmpty(animation.Name))
                {
                    throw new AssetLoadException(fileName, "At least one animation is missing a 'Name' parameter");
                }
                
                foreach (var frame in animation.Frames)
                {
                    if (frame[0] >= sprite.HorizontalFrames || frame[1] >= sprite.VerticalFrames)
                    {
                        var msg = "Frame '({0}, {1})' in animation '{2}' is invalid for the frames on the texture";
                        throw new AssetLoadException(fileName, String.Format(msg, frame[0], frame[1], animation.Name));
                    }
                }
            }
            
        }

        public IAnimationManager Animations
        {
            get { return this.animations; }
        }
    }
}

