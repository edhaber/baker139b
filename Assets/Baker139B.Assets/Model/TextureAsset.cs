using System;
using Baker139B.Assets.IF;

namespace Baker139B.Assets.Model
{
    public class TextureAsset : ITextureAsset
    {
        private readonly string fileName;
        private readonly int frameWidth;
        private readonly int frameHeight;
        private readonly int frameGameWidth;
        private readonly int frameGameHeight;
        private readonly int horizontalFrames;
        private readonly int verticalFrames;
        
        public TextureAsset(string fileName, int frameWidth, int frameHeight, int frameGameWidth, int frameGameHeight, int horizontalFrames, int verticalFrames)
        {
            this.fileName = fileName;
            this.frameWidth = frameWidth;
            this.frameHeight = frameHeight;
            this.frameGameWidth = frameGameWidth;
            this.frameGameHeight = frameGameHeight;
            this.horizontalFrames = horizontalFrames;
            this.verticalFrames = verticalFrames;
        }

        public string FileName
        {
            get { return this.fileName; }
        }
        
        public int FrameWidth
        {
            get { return this.frameWidth; }
        }
        
        public int FrameHeight
        {
            get { return this.frameHeight; }
        }

        public int FrameGameWidth
        {
            get { return this.frameGameWidth; }
        }
        
        public int FrameGameHeight
        {
            get { return this.frameGameHeight; }
        }
        
        public int VerticalFrames
        {
            get { return this.verticalFrames; }
        }
        
        public int HorizontalFrames
        {
            get { return this.horizontalFrames; }
        }
    }
}

