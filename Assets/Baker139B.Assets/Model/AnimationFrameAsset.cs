using System;
using Baker139B.Assets.IF;

namespace Baker139B.Assets.Model
{
    public class AnimationFrameAsset : IAnimationFrameAsset
    {
        private readonly int frameX;
        private readonly int frameY;
        
        public AnimationFrameAsset(int frameX, int frameY)
        {
            this.frameX = frameX;
            this.frameY = frameY;
        }
        
        public int FrameX
        {
            get { return this.frameX; }
        }
        
        public int FrameY
        {
            get { return this.frameY; }
        }
    }
}

