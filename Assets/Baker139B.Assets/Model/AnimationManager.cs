using System;
using System.Collections.Generic;
using System.Linq;
using Baker139B.Assets.Model;
using Baker139B.Assets.IF;

namespace Baker139B.Assets.Model
{
    public class AnimationManager : IAnimationManager
    {
        private readonly IDictionary<string, AnimationAsset> assets;
        
        public AnimationManager()
        {
            this.assets = new Dictionary<string, AnimationAsset>();
        }

        public void Add(string alias, AnimationAsset asset)
        {
            this.assets[alias] = asset;
        }
        
        public bool Contains(string alias)
        {
            return this.assets.ContainsKey(alias);
        }
        
        public IEnumerable<string> RequiredTextures
        {
            get
            {
                return (from animation in this.assets.Values 
                    select animation.Texture.FileName).Distinct();
                        
            }
        }
        
        public IAnimationAsset this[string alias]
        {
            get
            {
                return this.assets[alias];
            }
        }
    }
}

