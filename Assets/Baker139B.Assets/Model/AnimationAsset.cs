using System;
using Baker139B.Assets.IF;
using System.Collections.Generic;

namespace Baker139B.Assets.Model
{
    public class AnimationAsset : IAnimationAsset
    {
        private readonly TextureAsset textureAsset;
        private readonly IList<AnimationFrameAsset> frames;
        private readonly bool invertX;
        
        public AnimationAsset(TextureAsset textureAsset, IList<AnimationFrameAsset> frames, bool invertX)
        {
            if (null == textureAsset) throw new ArgumentNullException("textureAsset");
            if (null == frames) throw new ArgumentNullException("frames");
            
            this.textureAsset = textureAsset;
            this.frames = frames;
            this.invertX = invertX;
        }
        
        public ITextureAsset Texture
        {
            get { return this.textureAsset; }
        }

        public int FrameCount
        {
            get { return this.frames.Count; }
        }
        
        public IAnimationFrameAsset GetFrame (int index)
        {
            return this.frames[index];
        }
        
        public bool InvertX
        {
            get
            {
                return this.invertX;
            }
        }
    }
}

