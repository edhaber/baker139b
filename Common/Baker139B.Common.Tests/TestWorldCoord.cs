using System;
using NUnit.Framework;
using OpenTK;

namespace Baker139B.Common.Tests
{
    [TestFixture]
    public class TestWorldCoord
    {
        [Test]
        public void TestX()
        {
            var coord = new WorldCoord(1.3f, 1.6f);
            Assert.AreEqual(1.3f, coord.X);
        }
        
        [Test]
        public void TestY()
        {
            var coord = new WorldCoord(1.3f, 1.6f);
            Assert.AreEqual(1.6f, coord.Y);
        }
        
        [Test]
        public void TestEquals()
        {
            var coord = new WorldCoord(1.2f, 2.5f);
            var coordSame = new WorldCoord(1.2f, 2.5f);
            var coordDiffX = new WorldCoord(99.3f, 2.5f);
            var coordDiffY = new WorldCoord(1.2f, 99.3f);
            var coordDiffBoth = new WorldCoord(99.3f, 99.3f);
            
            Assert.IsTrue(coord.Equals(coordSame));
            Assert.IsFalse(coord.Equals(coordDiffX));
            Assert.IsFalse(coord.Equals(coordDiffY));
            Assert.IsFalse(coord.Equals(coordDiffBoth));
        }
        
        [Test]
        public void TestDistance()
        {
            var coord = new WorldCoord(1.2f, 2.5f);
            var coord2 = new WorldCoord(3.4f, 5.6f);
            Assert.AreEqual(3.80132f, WorldCoord.Distance(coord, coord2), 0.0001);
            Assert.AreEqual(3.80132f, WorldCoord.Distance(coord2, coord), 0.0001);
        }
        
        [Test]
        public void TestToString()
        {
            var coord = new WorldCoord(1.2f, 3.4f);
            Assert.AreEqual("WorldCoord(1.2, 3.4)", coord.ToString());
        }
        
        [Test]
        public void TestVectorAddition()
        {
            var coord = new WorldCoord(2.1f, 3.1f);
            var vector = new Vector2(1.2f, 3.4f);
            var result = coord + vector;
            Assert.AreEqual(new WorldCoord(3.3f, 6.5f), result);
        }
        
        [Test]
        public void TestSubtraction()
        {
            var coord = new WorldCoord(2, 3);
            var coord2 = new WorldCoord(4, 1);
            Assert.AreEqual(new Vector2(-2, 2), coord - coord2);
        }
    }
}

