using System;
using Baker139B.Common;
using NUnit.Framework;

namespace Baker139B.Common.Tests
{
    [TestFixture]
    public class TestTileCoord
    {
        [Test]
        public void TestTileX()
        {
            var tile = new TileCoord(5, 3);
            Assert.AreEqual(5, tile.X);
        }
        
        [Test]
        public void TestTileY()
        {
            var tile = new TileCoord(5, 3);
            Assert.AreEqual(3, tile.Y);
        }
        
        [Test]
        public void TestEquals()
        {
            var tile = new TileCoord(1, 2);
            var tileSame = new TileCoord(1, 2);
            var tileDiffX = new TileCoord(99, 2);
            var tileDiffY = new TileCoord(1, 99);
            var tileDiffBoth = new TileCoord(99, 99);
            
            Assert.IsTrue(tile.Equals(tileSame));
            Assert.IsFalse(tile.Equals(tileDiffX));
            Assert.IsFalse(tile.Equals(tileDiffY));
            Assert.IsFalse(tile.Equals(tileDiffBoth));
        }
        
        [Test]
        public void TestManhattanDistance()
        {
            var tileA = new TileCoord(1, 1);
            var tileB = new TileCoord(3, 4);
            Assert.AreEqual(5, TileCoord.ManhattanDistance(tileA, tileB));
            Assert.AreEqual(5, TileCoord.ManhattanDistance(tileB, tileA));
        }
        
        [Test]
        public void TestCrowDistance()
        {
            var tileA = new TileCoord(1, 1);
            var tileB = new TileCoord(3, 4);
            Assert.AreEqual(3.60555, TileCoord.Distance(tileA, tileB), 0.0001);
            Assert.AreEqual(3.60555, TileCoord.Distance(tileB, tileA), 0.0001);
        }
        
        [Test]
        public void TestToString()
        {
            var tile = new TileCoord(1, 2);
            Assert.AreEqual("TileCoord(1, 2)", tile.ToString());
        }
        
    }
}

