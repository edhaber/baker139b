using System;
using OpenTK;

namespace Baker139B.Common
{
    /// <summary>
    /// Represents an absolute world coordinate
    /// </summary>
    public struct WorldCoord
    {
        public readonly float X;
        public readonly float Y;
        
        public WorldCoord(float x, float y)
        {
            this.X = x;
            this.Y = y;
        }

        public static float Distance(WorldCoord a, WorldCoord b)
        {
            return (float)Math.Sqrt(Math.Pow(a.X - b.X, 2) + Math.Pow(a.Y - b.Y, 2));
        }
        
        public override string ToString()
        {
            return String.Format("WorldCoord({0}, {1})", this.X, this.Y);
        }
        
        // TODO: Eventually replace Vector2 with our own vector type
        public static WorldCoord operator +(WorldCoord a, Vector2 v)
        {
            return new WorldCoord(a.X + v.X, a.Y + v.Y);
        }

        public static Vector2 operator -(WorldCoord a, WorldCoord b)
        {
            return new Vector2(a.X - b.X, a.Y - b.Y);
        }
    }
}

