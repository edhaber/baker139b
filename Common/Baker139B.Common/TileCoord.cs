using System;

namespace Baker139B.Common
{
    /// <summary>
    /// Represents the notion of a "tile" in a 2D tile-based game.
    /// Note that this idea of a "tile" does not factor into any idea of an
    /// absolute game position. TileCoord(1,2) does not necessarily mean a
    /// corner position of (1.0, 2.0), nor does it mean a central position of
    /// (1.5, 2.5). The distance and manhattan distance algorithm can be thought
    /// of as the distance between the middle of two tiles.
    /// </summary>
    public struct TileCoord
    {
        public int X;
        public int Y;
        
        public TileCoord(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }
        
        public static int ManhattanDistance(TileCoord a, TileCoord b)
        {
            return Math.Abs(a.X - b.X) + Math.Abs(a.Y - b.Y);
        }

        public static double Distance(TileCoord a, TileCoord b)
        {
            return Math.Sqrt(Math.Pow(a.X - b.X, 2) + Math.Pow(a.Y - b.Y, 2));
        }
        
        public override string ToString()
        {
            return String.Format("TileCoord({0}, {1})", this.X, this.Y);
        }
    }
}

